local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(unitIDs)
	result = {}
	i = 1
	for key,value in pairs(unitIDs) do
		result[value] = i
		i = i + 1
	end
	return result
end