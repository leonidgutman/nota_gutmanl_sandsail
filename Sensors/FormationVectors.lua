local sensorInfo = {
	name = "FormationVectors",
	desc = "Returns a static array of formation vectors for 24 units excluding (0,0,0)",
	author = "Leonid Gutman",
	date = "2019-05-01",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	local formation_vectors = {}
	local spacing = 50
	for i = spacing*(-2),spacing*2,spacing do
		for j = spacing*(-2),spacing*2, spacing do
			if(i ~= 0 or j ~= 0) then 
				table.insert(formation_vectors, Vec3(i,0,j))
			end
		end
	end
	return formation_vectors
end